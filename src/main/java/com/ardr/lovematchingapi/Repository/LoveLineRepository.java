package com.ardr.lovematchingapi.Repository;

import com.ardr.lovematchingapi.entity.LoveLine;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LoveLineRepository extends JpaRepository<LoveLine, Long> {
}
