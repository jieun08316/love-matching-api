package com.ardr.lovematchingapi.Repository;


import com.ardr.lovematchingapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member, Long> {
}
