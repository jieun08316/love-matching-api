package com.ardr.lovematchingapi.model.member;


import jakarta.persistence.GeneratedValue;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberCreateRequest {
    private String name;
    private String phoneNumber;
    private Boolean isMan;
}
