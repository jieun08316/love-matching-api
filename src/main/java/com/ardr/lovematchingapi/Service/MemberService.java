package com.ardr.lovematchingapi.Service;


import com.ardr.lovematchingapi.Repository.MemberRepository;
import com.ardr.lovematchingapi.entity.Member;
import com.ardr.lovematchingapi.model.member.MemberCreateRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public Member getData(long id) {
        return memberRepository.findById(id).orElseThrow();
    }

    public void setMember(MemberCreateRequest request) {
        Member addData = new Member();
        addData.setName(request.getName());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setIsMan(request.getIsMan());
        memberRepository.save(addData);
    }
}
