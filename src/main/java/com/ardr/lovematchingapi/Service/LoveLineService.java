package com.ardr.lovematchingapi.Service;

import com.ardr.lovematchingapi.Repository.LoveLineRepository;
import com.ardr.lovematchingapi.entity.LoveLine;
import com.ardr.lovematchingapi.entity.Member;
import com.ardr.lovematchingapi.model.loveLine.LoveLineCreatRequest;
import com.ardr.lovematchingapi.model.loveLine.LoveLineItem;
import com.ardr.lovematchingapi.model.loveLine.LoveLinePhoneNumberChangeRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class
LoveLineService {
    private final LoveLineRepository loveLineRepository;

    public void setLoveLine(Member member, LoveLineCreatRequest request) {
        LoveLine addData = new LoveLine();
        addData.setMember(member);
        addData.setLovePhoneNumber(request.getLovePhoneNumber());
        loveLineRepository.save(addData);
    }

    public List<LoveLineItem> getLoveLines() {

        List<LoveLine> originList = loveLineRepository.findAll();
        List<LoveLineItem> result = new LinkedList<>();
        for (LoveLine loveLine : originList) {

            LoveLineItem addItem = new LoveLineItem();
            addItem.setMemberId(loveLine.getMember().getId());
            addItem.setMemberName(loveLine.getMember().getName());
            addItem.setMemberPhoneNumber(loveLine.getMember().getPhoneNumber());
            addItem.setLovePhoneNumber(loveLine.getLovePhoneNumber());
            result.add(addItem);
        }
        return result;

    }

    public void putPhoneNumber(long id, LoveLinePhoneNumberChangeRequest request){
        LoveLine originData = loveLineRepository.findById(id).orElseThrow();
        originData.setLovePhoneNumber(request.getLoveLinePhoneNumber());
        loveLineRepository.save(originData);
    }

}
