package com.ardr.lovematchingapi.controller;


import com.ardr.lovematchingapi.Service.LoveLineService;
import com.ardr.lovematchingapi.Service.MemberService;
import com.ardr.lovematchingapi.entity.Member;
import com.ardr.lovematchingapi.model.loveLine.LoveLineCreatRequest;
import com.ardr.lovematchingapi.model.loveLine.LoveLineItem;
import com.ardr.lovematchingapi.model.loveLine.LoveLinePhoneNumberChangeRequest;
import lombok.RequiredArgsConstructor;
import org.aspectj.apache.bcel.Repository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/love-line")
public class LoveLineController {

    private final MemberService memberService;
    private final LoveLineService loveLineService;

    @PostMapping("/new/member-id/{memberId}")
    public String setLoveLine(@PathVariable long memberId, @RequestBody LoveLineCreatRequest request) {
        Member member = memberService.getData(memberId);
        loveLineService.setLoveLine(member, request);
        return "OK";
    }


    @GetMapping("/all")
    public List<LoveLineItem> getLoveLines() {
        return loveLineService.getLoveLines();
    }

    @PutMapping("/phone-number/love-line-id/{loveLineId}")
    public String putPhoneNumber(@PathVariable long loveLineId, @RequestBody LoveLinePhoneNumberChangeRequest request){
        loveLineService.putPhoneNumber(loveLineId, request);
        return "OK";
    }

}

